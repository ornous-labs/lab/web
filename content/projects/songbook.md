---
templateKey: project
slug: songbook
name: Song Book
featuredMedia: /img/projects/songbook.jpg
description: >-
  A place to list and grade songs that are in my repertoire
---
What an amazing project
