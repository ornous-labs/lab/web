import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'react-emotion'

import Layout from '../components/Layout'
import Hero from '../components/Hero'

const Content = styled('div')`
  margin: 0 auto;
  max-width: 960px;
  padding: 0 3vw;
`

const GtdPage = ({
  data: {
    site: { siteMetadata: { title } },
    heroImage: {
      childImageSharp: { fluid: heroImageData },
    },
  },
}) => {
  return (
    <Layout
      hero={
        <Hero
          showProfilePhoto={false}
          heading="Getting Stuff Done"
          subHeading="Experiments for future web project"
          {...heroImageData}
        />
      }
    >
      <Helmet title={`GTD | ${title}`} />
      <Content>
        <p>GTD</p>
      </Content>
    </Layout>
  )
}

GtdPage.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        title: PropTypes.string.isRequired,
      }),
    }),
    heroImage: PropTypes.shape({
      childImageSharp: PropTypes.shape({}),
    }),
  }),
}

export default GtdPage

export const pageQuery = graphql`
  query GtdQuery {
    site {
      siteMetadata {
        title
      }
    }
    heroImage: file(relativePath: { eq: "projects/gtd.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1400, maxHeight: 850) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`
