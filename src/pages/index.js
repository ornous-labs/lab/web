import React from 'react'
import PropTypes from 'prop-types'
import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import styled from 'react-emotion'

import Layout from '../components/Layout/FullScreen'
import { HeroImage, Overlay } from '../components/Hero'

const Deck = styled('ul')`
  display: grid;
  list-style-type: none;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-auto-rows: minmax(290px, 31.5%);
  grid-gap: 0.9rem;
  padding: 0;
  margin: 0.9rem auto;
  width: calc(100vw - 3rem);
`

const Image = styled(Img)`
  max-height: 50%;
`

const Card = styled('li')`
  box-shadow: 4px 4px 3px 1px hsla(0, 0%, 20%, .5);
  background-color: floralwhite;
  font-size: 0.8rem;
  transition: transform 220ms ease-in, box-shadow 200ms ease-in;

  &:hover {
    box-shadow: 3px 3px 3px 1px hsla(0, 0%, 30%, .5);
    transform: scaleX(1.02) scaleY(1.01);
  }

  &:nth-child(3) {
    grid-column: span 2;
    grid-row: span 2;
  }

  &:nth-child(1), &:nth-child(3), &:nth-child(10), &:nth-child(13) {
    grid-column: span 2;
  }

  &:nth-child(2) {
    grid-row: span 2
  }


  a:hover {
    img {
      filter: sepia(0%);
    }
  }

  img {
    transition: filter 250ms ease;
    filter: sepia(60%);
  }

  h3, p {
    padding: 0 1em;
  }
`

// const BackgroundImage = styled(Hero)`
//   .gatsby-image-wrapper {
//     position: absolute;
//     top: 0;
//     left: 0;
//     right: 0;
//     bottom: 0;
//     height: 100%;
//   }
// `

const HeroImageContainer = styled('div')`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
`

const BackgroundImage = ({ children, ...imgProps }) => {
  return (
    <div>
      <HeroImageContainer>
        <HeroImage src={imgProps.src} sizes={imgProps} />
      </HeroImageContainer>
      <Overlay>{children}</Overlay>
    </div>
  )
}


const IndexPage = ({ data }) => {
  let projects = data.allMarkdownRemark.edges
  const heroImageData = data.hero.childImageSharp.fluid

  projects = projects.concat(projects).concat(projects).slice(0, 14)

  return (
    <Layout>
      <BackgroundImage heading="Projects List" {...heroImageData}>
        <Deck>
        {projects.map(({ project }, i) => {
          const gtdImageData = project.frontmatter.featuredMedia.childImageSharp.fluid
          return (
            <Card key={project.fields.slug + i} className={project.frontmatter.slug}>
              <Link to='/gtd'>
                <Image
                  src={gtdImageData.src}
                  sizes={gtdImageData}
                  />
                <h3>{project.frontmatter.name}</h3>
                <p>{project.frontmatter.description}</p>
              </Link>
            </Card>
          )
        })}
        </Deck>
      </BackgroundImage>
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({ }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexQuery {
    hero: file(relativePath: { eq: "tree-blossom.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1400, maxHeight: 850) {
          ...GatsbyImageSharpFluid_withWebp_tracedSVG
        }
      }
    }
    allMarkdownRemark(
      limit: 6,
      filter: { frontmatter: { templateKey: { eq: "project" } } }
    ) {
      edges {
        project: node {
          fields {
            slug
          }
          frontmatter {
            slug
            featuredMedia {
              childImageSharp {
                fluid(maxWidth: 550, maxHeight: 350, cropFocus: SOUTH) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            name
            description
          }
        }
      }
    }
  }
`
