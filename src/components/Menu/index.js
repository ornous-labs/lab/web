import React from 'react'
import { Link } from 'gatsby'
import styled from 'react-emotion'
import { transparentize } from 'polished'

import Logo from '../Logo'
import Hamburger from './Hamburger'
import { Heading } from './Heading'
import { NavLink } from './NavLink'
import { List as MenuList } from './List'

const MenuContainer = styled('nav')`
  display: grid;
  grid-template-areas:
    'heading spacer hamburger'
    'menu menu menu';
  grid-template-columns: 190px auto 40px;
  height: 3rem;
  line-height: 3rem;
  background-color: ${({ theme }) => transparentize(0.15, theme.panelBgColor)};
  box-shadow: 0 3px 6px hsla(0, 0%, 0%, 0.4);
  grid-area: hero;

  @media (min-width: 700px) {
    grid-template-areas: 'heading menu';
    grid-template-columns: 190px minmax(300px, 1fr);
  }

  @media (min-width: 840px) {
    grid-template-columns: 190px minmax(300px, 1fr) 190px;
  }
`

const SubHeading = styled('span')`
  padding: 0 12px;
`

const HomeLink = styled(Link)`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-items: center;
  height: inherit;

  &:visited {
    color: ${({ theme }) => theme.ornousColor};
  }
`

const Menu = () => (
  <MenuContainer>
    <Heading>
      <HomeLink to="/">
        <Logo />
        <SubHeading>ornous</SubHeading>
      </HomeLink>
    </Heading>
    <MenuList>
      <NavLink to="/">Home</NavLink>
      <NavLink to="/blog">Blog</NavLink>
      <NavLink to="/books">Reading</NavLink>
      <NavLink to="/about">About&nbsp;Me</NavLink>
    </MenuList>
    <Hamburger />
  </MenuContainer>
)

export default Menu
