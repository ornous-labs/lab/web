import React from 'react'
import styled from 'react-emotion'
import { rgba, lighten } from 'polished'
import { Link } from 'gatsby'

const Wrapper = styled('li')`
  margin-bottom: 0;
  display: flex;
  flex: 0 1;
  height: 100%;
`

const StyledLink = styled(Link)`
  display: inline-block;
  position: relative;
  padding: 0 1.3em;
  opacity: 0.9;
  color: ${({ theme }) => theme.menuFgColor};
  text-decoration: none;
  letter-spacing: 0.16rem;
  text-transform: uppercase;
  font-size: 1rem;
  font-weight: bold;
  transition: background-color 0.2s ease-in-out, opacity 0.1s ease-in;

  &:after {
    content: '';
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    background-color: #873e4c;
    height: 4px;
    width: 100%;
    transform: scaleX(0);
  }

  &:hover,
  &:focus {
    outline: 0;
    &:after {
      transition: transform ease 250ms;
      transform: scaleX(1);
    }
  }

  &.active {
    box-shadow: inset 0px -4px 0px ${({ theme }) => lighten(0.1, theme.menuFgColor)};
    background-color: hsla(0, 0%, 30%, 0.1);
  }

  &:hover {
    background-color: ${({ theme }) => rgba(theme.menuFgColor, 0.09)};
    opacity: 1;
  }

  &:visited {
    color: ${({ theme }) => theme.ornousColor};
  }
`

export const NavLink = ({ children, ...props }) => (
  <Wrapper>
    <StyledLink {...props} activeClassName="active">
      {children}
    </StyledLink>
  </Wrapper>
)
