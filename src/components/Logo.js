import React from 'react'
import styled from 'react-emotion'

const Logo = () => <Img src="/img/logo.png" alt="Oz" />

const Img = styled('img')`
  margin: 0;
  height: 80%;
`

export default Logo
