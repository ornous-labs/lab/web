import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'
import { desaturate, lighten } from 'polished'
import Img from 'gatsby-image'

export const HeroImage = styled(Img)`
  grid-area: hero;
  max-width: 100vw;
  z-index: -1;
`

const Content = styled('div')`
  width: 98vw;
  min-width: 98vw;
  max-width: 1300px;
  margin: 0 auto;
  padding: 1vw;
`

export const Overlay = styled('div')`
  grid-area: hero;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: hsla(0, 0%, 100%, 0.5);
  height: 100%;
`

const Heading = styled('h2')`
  // text-shadow: -2px -2px hsla(0, 0%, 0%, 0.75);
  filter: drop-shadow(0px 0px 2px hsla(0, 0%, 13%, 1));
  -webkit-text-stroke: 1px hsla(348, 25%, 42%, .9);
  letter-spacing: 0.120rem;
  font-size: 2rem;
  color: ${() => desaturate(0.25, lighten(0.10, '#e2b091'))};
`

const SubHeading = styled('h3')`
  // text-shadow: -2px -2px hsla(0, 0%, 0%, 0.6);
  filter: drop-shadow(0px 0px 1px hsla(0, 0%, 40%, 1));
  letter-spacing: 0.09rem;
  font-size: 1.4rem;
  margin: 0;
  color: ${() => lighten(0.18, '#e2b091')};
`

const Hero = ({ children, heading, subHeading, className, ...imgProps }) => {
  return (
    <React.Fragment>
      <HeroImage src={imgProps.src} sizes={imgProps} />
      <Overlay>
        <Content>
          <Heading>{heading}</Heading>
          <SubHeading>{subHeading}</SubHeading>
          {children && (<div>{children}</div>)}
        </Content>
      </Overlay>
    </React.Fragment>
  )
}

Hero.propTypes = {
  heading: PropTypes.string,
  subHeading: PropTypes.string,
}

Hero.defaultProps = {
  heading: null,
  subHeading: null,
}

export default Hero
