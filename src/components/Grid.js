import styled from 'react-emotion'

export const GridContainer = styled('div')`
`

export const Grid = styled('div')`
  height: 100%;
  display: grid;
  row-gap: 2rem;
  grid-template-areas: 'header' 'main' 'footer';
  grid-template-rows: min-content auto max-content;
  margin: 0;
  padding: 0;
`

export const MainArea = styled('section')`
  grid-area: main;
  color: ${({ theme }) => theme.textColor};
  width: 100%;

  a {
    color: ${({ theme }) => theme.ornousColor}

    &:visited {
      color: ${({ theme }) => theme.ozzyColor}
    }
  }
`

export const Header = styled('header')`
  grid-area: header;
  position: relative;
  margin: 0;
  display: grid;
  grid-template-areas: 'hero';
  overflow: hidden;
`
