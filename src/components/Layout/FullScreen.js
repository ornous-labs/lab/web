import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import { ThemeProvider } from 'emotion-theming'

import { MainArea } from '../Grid'
import theme from '../../themes'


const Layout = ({ children, hero }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQueryFS {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          link={[
            {
              rel: 'icon',
              href: data.site.siteMetadata.favicon,
              type: 'image/x-icon',
            },
          ]}
          meta={[
            { name: 'description', content: 'Ornous' },
            { name: 'keywords', content: 'Ornous, Ousmane, Ndiaye, developer' },
          ]}
        >
          <html lang="en" />
        </Helmet>
        <ThemeProvider theme={theme}>
          <MainArea>{children}</MainArea>
        </ThemeProvider>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
