import facepaint from 'facepaint'
import { injectGlobal } from 'emotion'
import { normalize, darken, lighten } from 'polished'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faGithub,
  faGitlab,
  faLinkedin,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'

import warmAntique from './warm-antique'

library.add(faBars, faGithub, faGitlab, faLinkedin, faTwitter)

const theme = {
  ...warmAntique.colors,
  ...warmAntique,
}

// TODO: Extract breakpoints and facepaint
const breakpoints = [576, 768, 992, 1200]
const mq = facepaint(breakpoints.map(bp => `@media (min-width: ${bp}px)`))

injectGlobal`
  ${normalize()}
  html {
    ${mq({
      fontSize: ['100%', '112.5%'],
    })}
  }

  h1 {
    color: ${theme.menuFgColor};
    font-size: 1.4rem;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: ${theme.typefaces.heading};
  }

  body {
    font-family: ${theme.typefaces.legibleText};
  }

  a {
    color: ${theme.menuFgColor};
    text-decoration: none;

    &:hover {
      color: ${darken(0.1, theme.menuFgColor)};
    }

    &:visited {
      color: ${lighten(0.1, theme.menuFgColor)};
    }
  }
`

export default theme
