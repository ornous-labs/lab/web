import React from 'react'
import { shallow } from 'enzyme'
import 'enzyme-to-json'
import HomePage from '../pages/index'

const createWrapper = props => shallow(<HomePage {...props} />)
const createTestProps = (props = {}) => ({
  data: {
    hero: {
      childImageSharp: {
        fluid: {
          sizes: {
            aspectRatio: 1,
          },
        },
      },
    },
    allMarkdownRemark: {
      edges: [
        {
          project: {
            id: 1,
            fields: {
              slug: 'project-slug',
            },
            frontmatter: {
              name: 'Some Project',
              description: 'Some Project Description',
              featuredMedia: {
                childImageSharp: {
                  fluid: {
                    sizes: {
                      aspectRatio: 1,
                    },
                  },
                },
              },
            },
          },
        },
      ],
    },
  },
  location: { pathname: '/' },
  ...props,
})

describe('HomePage', () => {
  let wrapper
  beforeEach(() => (wrapper = createWrapper(createTestProps())))
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
